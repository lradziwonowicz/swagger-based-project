# Sample project for Play 2 and Swagger UI

You can run the sample app:

````
>play
>run 5000
````

The application will listen on port 5000 and respond to `http://localhost:5000/assets/swagger-ui/index.html`
