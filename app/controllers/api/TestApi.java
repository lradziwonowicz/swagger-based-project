package controllers.api;

import javax.ws.rs.GET;

import models.ApiResponse;
import play.mvc.Result;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Manage tasks related operations.
 */
@Api(value = "/api/test", description = "Test operation")
public class TestApi extends BaseApiController {

	/**
	 * Get test object
	 */
	@GET
	@ApiOperation(value = "Test object", notes = "Get test object")
	public static Result index() {
		return JsonResponse(new ApiResponse(200, "Test object"));
	}
}
